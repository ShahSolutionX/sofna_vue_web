import AboutPage from "@/views/AboutPage.vue";
import AgentDetails from "@/views/AgentDetails.vue";
import Agents from "@/views/Agents.vue";
import Booking from "@/views/Booking.vue";
import Customer from "@/views/Customer.vue";
import CustomerDetail from "@/views/CustomerDetail.vue";
import HomePage from "@/views/HomePage.vue";
import LoginPage from "@/views/LoginPage.vue";
import Packages from "@/views/Packages.vue";
import Payments from "@/views/Payments.vue";
import Swal from "sweetalert2"; // Import SweetAlert2
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    component: HomePage,
    meta: { requiresAuth: true, roles: ["Agent", "Admin"] },
  },
  {
    path: "/about",
    component: AboutPage,
    meta: { requiresAuth: true, roles: ["Agent", "Admin"] },
  },
  {
    path: "/agents",
    component: Agents,
    meta: { requiresAuth: true, roles: ["Admin"] },
  },
  {
    path: "/agent-detail/:id?",
    component: AgentDetails,
    name: "AgentDetails",
    meta: { requiresAuth: true, roles: ["Admin"] },
  },
  {
    path: "/packages",
    component: Packages,
    meta: { requiresAuth: true, roles: ["Agent", "Admin"] },
  },
  {
    path: "/bookings",
    component: Booking,
    meta: { requiresAuth: true, roles: ["Agent", "Admin"] },
  }, // Example roles
  {
    path: "/customers",
    component: Customer,
    meta: { requiresAuth: true, roles: ["Admin", "Agent"] },
  },
  {
    path: "/payments",
    component: Payments,
    meta: { requiresAuth: true, roles: ["Admin"] },
  },
  {
    path: "/customer-detail/:id",
    component: CustomerDetail,
    name: "CustomerDetail",
    meta: { requiresAuth: true, roles: ["Admin", "Agent"] },
  },
  { path: "/login", component: LoginPage },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    const userRole = localStorage.getItem("userRole");

    if (!userRole) {
      Swal.fire({
        // Use Swal.fire()
        icon: "warning",
        title: "Authentication Required",
        text: "You must be logged in to access this page.",
      }).then(() => {
        next("/login"); // Redirect after Swal is closed
      });
      console.log("No user role found");
    } else if (to.meta.roles && !to.meta.roles.includes(userRole)) {
      next("/");
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
